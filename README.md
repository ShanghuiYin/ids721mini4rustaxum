
# IDS721 Spring 2024 Weekly Mini Project 4 Rust Axum Greedy Coin Microservice

## Local Demo

![img.png](img.png)

## Requirements
1. Containerize simple Rust Actix web app 
2. Build Docker image 
3. Run container locally

## Build and Run
1. Install Rust
2. Start by creating a new binary-based Cargo project and changing into the new directory:
   `cargo new actix-web-service && cd actix-web-service`
3. Implement the logic in `main.rs` file
4. Add the dependencies in `Cargo.toml` file
5. Build the project `cargo build`
6. Run the project `cargo run`
7. Access the service using the URL `http://localhost:3000/change/1/34`
8. Or using curl `curl http://localhost:3000/change/1/34`

![img_1.png](img_1.png)

## Dockerize

### Build

```bash
docker build -t myimage .
```

![img_2.png](img_2.png)

### Docker App
![img_3.png](img_3.png)

## Run

```bash
docker run -dp 3000:3000 myimage
```

### Stop

```bash
docker ps # find image id
docker stop `CONTAINER ID` # replace with image id to stop
```

## Reference

